'use strict';

var gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    browserSync = require('browser-sync'),
    reload = browserSync.stream,
    jade = require('gulp-jade'),
    compass = require('gulp-compass'),
    autoprefixer = require('gulp-autoprefixer'),
    shorthand = require('gulp-shorthand'),
    cssmin = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify'),
    replace = require('gulp-replace'),
    inlinesource = require('gulp-inline-source');

var path = {
    src: {
        html: 'src/*.jade',
        style: 'src/style/*.scss',
        fonts: 'src/fonts/**/*.*',
        img: ['src/img/**/*', '!src/img/sprites/**/*', '!src/img/sprites'],
        js: 'src/js/*.js',
        php: 'src/php/**/*',
        other: ['src/*.*', 'src/.htaccess', '!src/*.jade'],
        video: 'src/video/*.*'
    },
    watch: {
        html: ['src/*.jade', 'src/templates/**/*.jade'],
        style: 'src/style/**/*.scss',
        css: 'build/css/**/*.css',
        fonts: 'src/fonts/**/*.*',
        img: 'src/img/**/*',
        js: 'src/js/**/*',
        php: 'src/php/**/*',
        other: ['src/*.*', 'src/.htaccess', '!src/*.jade'],
        video: 'src/video/*.*'
    },
    build: {
        html: 'build/',
        style: 'build/css',
        fonts: 'build/fonts',
        img: 'build/img',
        js: 'build/js',
        php: 'build/php',
        other: 'build/',
        video: 'build/video'
    }
}

gulp.task('server-php', function() {
    connect.server({
        base: 'build',
        port: 8000,
        keepalive: true
    });
});

gulp.task('server', ['server-php'], function () {
    browserSync({
        proxy: 'localhost:8000',
        port: 9000,
        open: false,
        logPrefix: 'o-webserver'
    });
});

gulp.task('jade:build', function() {
    return gulp.src(path.src.html)
        .pipe(jade({
            pretty: true,
            baseDir: 'src/'
        }))
        .pipe(rename({
            extname: '.php'
        }))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload());
});

gulp.task('compass:watch', function() {
    return gulp.src(path.src.style)
        .pipe(compass({
            config_file: 'config.rb',
            css: 'build/css',
            sass: 'src/style',
            task: 'watch',
            sourcemap: true
        }))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 1000 versions'],
            cascade: true
        }))
        // .pipe(shorthand())
        .pipe(gulp.dest(path.build.style))
        .pipe(cssmin())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest(path.build.style))
        .pipe(reload());
});

gulp.task('compass:build', function() {
    return gulp.src(path.src.style)
        .pipe(compass({
            config_file: 'config.rb',
            css: 'build/css',
            sass: 'src/style',
            task: 'compile'
        }))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 1000 versions'],
            cascade: true
        }))
        // .pipe(shorthand())
        .pipe(gulp.dest(path.build.style))
        .pipe(cssmin())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest(path.build.style))
        .pipe(reload());
});

gulp.task('js:build', function () {
    return gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload());
});

gulp.task('php:build', function () {
    return gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
        .pipe(reload());
});

gulp.task('other:build', function () {
    return gulp.src(path.src.other)
        .pipe(gulp.dest(path.build.other))
        .pipe(reload());
});

gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload());
});

gulp.task('img:build', function() {
    return gulp.src(path.src.img)
        .pipe(imagemin({
            // svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true,
            progressive: true,
            multipass: true

        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload());
});

gulp.task('video:build', function () {
    return gulp.src(path.src.video)
        .pipe(gulp.dest(path.build.video))
        .pipe(reload());
});

gulp.task('watch', ['compass:build'], function() {
    gulp.watch(path.watch.html, ['jade:build']);
    gulp.watch(path.watch.fonts, ['fonts:build']);
    gulp.watch(path.watch.img, ['img:build']);
    gulp.watch(path.watch.php, ['php:build']);
    gulp.watch(path.watch.other, ['other:build']);
    gulp.watch(path.watch.video, ['video:build']);
    gulp.watch(path.watch.css, function() {
        return gulp.src(path.watch.css)
            .pipe(reload());
    });
    gulp.watch(path.watch.js, ['js:build']);
});

gulp.task('clean', function() {
    del('build/');
});

gulp.task('optimize', function() {
    return gulp.src(['build/*.html', 'build/*.php'])
        .pipe(inlinesource())
        .pipe(replace('url(\'../', 'url(\''))
        .pipe(replace('url(../', 'url('))
        .pipe(gulp.dest('build/'));
});

gulp.task('default', [
    'img:build',
    'fonts:build',
    'compass:watch',
    'jade:build',
    'js:build',
    'php:build',
    'other:build',
    'video:build',
    'server',
    'watch'
]);

gulp.task('build', [
    'img:build',
    'fonts:build',
    'compass:build',
    'jade:build',
    'js:build',
    'php:build',
    'other:build',
    'video:build'
]);
