/*-----------------------------------------------------------------------------
    Модальные окна
-----------------------------------------------------------------------------*/
;$(function() {
    var isScrolled = false;
    var isShowed = false;

    $('.show-popup').fancybox({
        padding: 0,
        autoCenter: false,
        fitToView: false,
        margin: 10,
        scrolling: 'no',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.show-img-popup').fancybox({
        padding: 0,
        autoCenter: true,
        margin: 10,
        scrolling: 'no',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $.fancybox.open({
        content: $('#sale')
    });
});
/*-----------------------------------------------------------------------------
    /Модальные окна
-----------------------------------------------------------------------------*/
