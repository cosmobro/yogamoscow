/*-----------------------------------------------------------------------------
    Логика интерфейса
-----------------------------------------------------------------------------*/
;
$(function() {
    // Функция добавляет подсказку о скидке в селектбоксы
    function addDiscountLabelToSelect() {
        if ($(this).hasClass('__discount') && $(this).find('select').val() > 1 && !$(this).hasClass('__discounted')) {
            $(this).addClass('__discounted').append('<span class="select_discount">Скидка 50%</span>');
        } else if ($(this).hasClass('__discount') && $(this).find('select').val() < 2) {
            $(this).removeClass('__discounted').find('.select_discount').remove();
        }
    }

    // Ввод номера по маске
    $('input[name=phone]').mask('+7 (999) 999 99 99');

    // Стилизация селектов, чекбоксов, радиокнопок
    $('.select, .checkbox, .radio').styler({
        onSelectClosed: addDiscountLabelToSelect
    });
    $('.select').each(addDiscountLabelToSelect);

    // Прятание расписание в форме заказа
    $('.registration_shedule_trigger').click(function() {
        if ($('.registration_shedule').hasClass('__opened')) {
            $('.registration_shedule').removeClass('__opened');
            $(this).text('Показать расписание');
        } else {
            $('.registration_shedule').addClass('__opened');
            $(this).text('Свернуть расписание');
        }
    });

    // Изменение цвета даты при выборе
    $('div.calendar_calendar_checkbox').change(function() {
        var dayElement = $(this).parent();
        if ($(this).is('.checked')) {
            dayElement.addClass('__checked');
        } else {
            dayElement.removeClass('__checked');
        }
    });

    // Логика поля выбора дня
    // $('.it.__calendar').focus(function() {
    //     var targetElem = $(this);
    //     if ($(this).hasClass('__select')) {
    //         $('#calendar .calendar_head_hint_num').text('');
    //         $.fancybox($('#calendar'), {
    //             padding: 0,
    //             autoCenter: false,
    //             fitToView: false,
    //             margin: 10,
    //             scrolling: 'no',
    //             helpers: {
    //                 overlay: {
    //                     locked: false
    //                 }
    //             },
    //             beforeShow: function() {
    //                 // Логика работы календаря
    //                 $('.calendar_form').trigger('reset');
    //                 $('.calendar_form').submit(function(e) {
    //                     var daysText = '',
    //                         formDataArr = $(this).serializeArray();
    //                     e.preventDefault();
    //                     $(this).removeClass('__select');
    //                     for (var i = 0; i < formDataArr.length; i++) {
    //                         daysText += formDataArr[i].value + ', ';
    //                     }
    //                     targetElem.val(daysText);
    //                     $.fancybox.close();
    //                     return false;
    //                 })
    //             }
    //         });
    //     } else {
    //         $('#calendar .calendar_head_hint_num').text('4');
    //         $.fancybox($('#calendar'), {
    //             padding: 0,
    //             autoCenter: false,
    //             fitToView: false,
    //             margin: 10,
    //             scrolling: 'no',
    //             helpers: {
    //                 overlay: {
    //                     locked: false
    //                 }
    //             },
    //             beforeShow: function() {
    //                 // Логика работы календаря
    //                 $('.calendar_form').submit(function(e) {
    //                     e.preventDefault();

    //                     $.fancybox.close();
    //                     return false;
    //                 })
    //             }
    //         });
    //     }
    // });

});
/*-----------------------------------------------------------------------------
    /Логика интерфейса
-----------------------------------------------------------------------------*/
