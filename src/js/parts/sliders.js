;$(function() {
    // Слайдер месяцев
    $('.calendar_pager_pages').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.calendar_months',
        arrows: true,
        centerMode: true,
        infinite: false,
        prevArrow: '.calendar_pager-arrows_arrow.__prev',
        nextArrow: '.calendar_pager-arrows_arrow.__next',
        focusOnSelect: true
    });
    // Слайдер календаря
    $('.calendar_months').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.calendar_pager_pages',
        infinite: false
    });
    // Слайдер тренеров
    $('.trainer_slider_slides').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '.trainer_slider_arrow.__prev',
        nextArrow: '.trainer_slider_arrow.__next',
    });
});