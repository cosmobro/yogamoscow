/*-----------------------------------------------------------------------------
    Открыватель меню
-----------------------------------------------------------------------------*/
$(function() {
    $('#menu-btn_checkbox').change(function() {
        if ($(this).is(':checked')) {
            $('.header').addClass('__opened-nav');
            $('.header_i:not(.__right,.__left)').fadeOut(150, function() {
                $('.navigation, .offer, .video-bg').addClass('__opened');
                $('.navigation_list, .navigation_addr').animate({
                    opacity: 1
                }, 150);
            });
        } else {
            $('.header').removeClass('__opened-nav');
            $('.navigation_list, .navigation_addr').animate({
                opacity: 0,
                display: 'none'
            }, 150, function() {
                $('.navigation, .offer, .video-bg').removeClass('__opened');
                $('.header_i:not(.__right,.__left)').fadeIn(150);
            });
        }
    });
    $('#menu-fixed-btn_checkbox').change(function() {
        if ($(this).is(':checked')) {
            $('.navigation-fixed_list').slideDown();
        } else {
            $('.navigation-fixed_list').slideUp();
        }
    });

    function closeMenuonMobile() {
        if ($(window).width() >= 768) {
            $('.navigation-fixed_list').css('display', 'block');
            $('#menu-fixed-btn_checkbox:checked').prop('checked', false);
        } else {
            $('.navigation-fixed_list').css('display', 'none');
        }
    }
    closeMenuonMobile();
    $(window).resize(closeMenuonMobile);
});
/*-----------------------------------------------------------------------------
    /Открыватель меню
-----------------------------------------------------------------------------*/
