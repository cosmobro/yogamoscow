/*-----------------------------------------------------------------------------
    Работа с формами
-----------------------------------------------------------------------------*/
$(function () {
    $('input[name=phone]').mask('+7 999 999 99 99');
    $('form').each(function() {
        $(this).validate({
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    required: true,
                },
                mail: {
                    required: true,
                    email: false
                },
            },
            messages: {
                name: {
                    required: 'Заполните поле',
                },
                phone: {
                    required: 'Заполните поле',
                },
                mail: {
                    required: 'Заполните поле',
                },
            },
            errorClass: '__error',
            validClass: '__valid',
            errorElement: 'span',
            errorPlacement: function(error, element) {
                element.attr('placeholder', error.text());
            },
            submitHandler: function(form) {
                //console.log($(form).serialize());
                var goal = $(form).data('goal');
                $.ajax({
                    type: 'POST',
                    url: 'php/send.php',
                    data: $(form).serialize(),
                    success: function(data) {
                        console.log(data);
                        //console.log(goal);
                        yaCounter37420420.reachGoal(goal);
                        $.fancybox.close();
                        $.fancybox({
                            content: $('#thanks').show(),
                            padding: 0
                        });
                        // setTimeout(function() {
                        //     $.fancybox.close();
                        // }, 7000);
                    },
                    error: function(data) {
                        // console.log(data);
                        $.fancybox.close();
                        $.fancybox({
                            content: $('#error').show(),
                            padding: 0
                        });
                        setTimeout(function() {
                            $.fancybox.close();
                        }, 7000);
                    }
                });
            }
        });
    });
});
/*-----------------------------------------------------------------------------
    /Работа с формами
-----------------------------------------------------------------------------*/