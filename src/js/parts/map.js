/*-----------------------------------------------------------------------------
    Карта
-----------------------------------------------------------------------------*/
;(function() {
    var myMap,
        addr = 'г. Москва, Пресненская наб., 6с2',
        placemark;
    ymaps.ready(function() {
        ymaps.geocode(addr).then(function(res) {
            var coordinates = res.geoObjects.get(0).geometry.getCoordinates();
            // Создание карты
            myMap = new ymaps.Map('map', {
                center: coordinates,
                zoom: 14,
                controls: ['zoomControl'],
                behaviors: ['dblClickZoom', 'multiTouch', 'rightMouseButtonMagnifier']
            }, {
                searchControlProvider: 'yandex#search'
            });
            myMap.events.add('click', function() {
                console.log(myMap.behaviors.enable('drag'));
            });
            placemark = new ymaps.Placemark(coordinates, {
                hintContent: addr,
                balloonContent: '<b style="font-weight:700;">Москва, Пресненская наб., 6с2</b><br /><br /><b style="font-weight:700;">Расписание:</b><br />Понедельник, четверг 19:30-21:00<br />Вторник, пятница 19:30-21:00',
                balloonContentHeader: 'Йога в Москва Сити'
            }, {
                iconLayout: 'default#image',
                iconImageHref: 'img/marker.png',
                iconImageSize: [60, 72],
                iconImageOffset: [-30, -72]
            });
            myMap.geoObjects.add(placemark);
        });
    });
})();
/*-----------------------------------------------------------------------------
    /Карта
-----------------------------------------------------------------------------*/
