/*-----------------------------------------------------------------------------
    Подключение библиотек и плагинов
-----------------------------------------------------------------------------*/

// Modernizr
//= ../../bower_components/modernizr/modernizr.js

// Jquery
//= ../../bower_components/jquery/dist/jquery.js

// Теперь можно подключать jquery плагины :)
//= ./parts/plugins.js

// Jquery maskedinput
//= ../../bower_components/jquery.maskedinput/dist/jquery.maskedinput.js

// Form Styler
//= ../../bower_components/jquery-form-styler/jquery.formstyler.js

// Slick
//= ../../bower_components/slick-carousel/slick/slick.js

// Fancybox
//= ../../bower_components/fancybox/source/jquery.fancybox.js

// Validate
//= ../../bower_components/jquery-validation/dist/jquery.validate.js

/*-----------------------------------------------------------------------------
    /Подключение библиотек и плагинов
-----------------------------------------------------------------------------*/

// TODO: навести порядок

//= ./parts/loader.js
//= ./parts/video-looper.js
//= ./parts/ui.js
//= ./parts/open-menu.js
//= ./parts/sliders.js
//= ./parts/map.js
//= ./parts/modals.js
//= ./parts/scrollTo.js
//= ./parts/forms.js
//= ./parts/fixMenu.js
