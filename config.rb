# Подключение плагинов компасса
require 'compass/import-once/activate'
# Фикс rgba фона для ie8
require 'rgbapng'

# Пути
http_path = 'build'
css_dir = 'build/css'
sass_dir = 'src/style'
images_dir = 'build/img'
javascripts_dir = '/build/js'
fonts_dir = 'build/fonts'
sprite_load_path = 'src/img/sprites'

# Настройки компилятора
output_style     = :expanded
relative_assets  = true
line_comments    = false
preferred_syntax = :scss
